import React, { PureComponent } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import App from "./app/app.index";
import { MuiThemeProvider } from "@material-ui/core";
import { EveRoutes } from "../types";
import { Dispatch, bindActionCreators } from "redux";
import { connect } from "react-redux";
import { darkTheme, lightTheme } from "../hoc/mui-theme";


class AppRoot extends PureComponent<any> {
  render() {
    const { settings: { theme } } = this.props;

    return (
      <MuiThemeProvider theme={theme === 'dark' ? darkTheme : lightTheme}>
        <BrowserRouter basename="/">
          <Switch>
            <Route path="/" exact={false} component={App} />
          </Switch>
        </BrowserRouter>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = (state: EveRoutes.Store) => {
  return {
    settings: { ...state.settings },
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AppRoot);
