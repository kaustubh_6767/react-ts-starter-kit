import React, { PureComponent, lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import Loader from '../../common/components/loader/loader.component';
import NotFound from '../../common/components/not-found/not-found.component';
import SecureRoute, { SecureRouteProps } from '../../common/components/secure-route/secure-route.component';
import Home from './home/container/home.container';
import Login from './login/container/login.container';

interface Props {
  isAuthenticated: boolean;
}

export default class AppRoutes extends PureComponent<Props> {

  routes: SecureRouteProps[] = [
    {
      path: '',
      exact: true,
      component: Home,
      authorizedRoles: []
    },
    // {
    //   component: NotFound,
    //   authorizedRoles: []
    // }
  ]

  loadSecureRoutes = () => {
    return (
      <div>
        <Suspense fallback={<Loader />}>
          <Switch>
            {
              this.routes.map((route: SecureRouteProps) => {
                return (
                  <SecureRoute path={route.path} component={route.component} authorizedRoles={route.authorizedRoles} />
                )
              })
            }
          </Switch>
        </Suspense>
      </div>
    )
  }

  render() {
    const { isAuthenticated } = this.props;
    if (!isAuthenticated) {
      return (
        <Switch>
          <Route path="" component={Login} />
          <Redirect to="" />
        </Switch>
      )
    }
    return this.loadSecureRoutes();
  }
}