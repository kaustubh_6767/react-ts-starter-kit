import React, { PureComponent } from 'react';
import styles from './login.module.scss';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class Login extends PureComponent {

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.paperContainer}>
                    <div className={styles.header}>
                        <span>Login</span>
                    </div>
                    <form>
                        <Paper elevation={2} className={styles.paper}>
                            <div className={styles.formComponent}>
                                <TextField fullWidth id="username" label="Username" variant="outlined" />
                            </div>
                            <div className={styles.formComponent}>
                                <TextField fullWidth type="password" id="password" label="Password" variant="outlined" />
                            </div>
                        </Paper>
                        <div className={styles.action}>
                            <Button variant="contained" color="primary">
                                Login
                            </Button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login;
