import React, { PureComponent } from 'react';
import styles from './test.module.scss';

class Test extends PureComponent {
    render() {
        return (
            <div className={styles.container}>
                Test Works!
            </div>
        )
    }
}

export default Test;
