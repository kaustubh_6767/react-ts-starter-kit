import React, { PureComponent } from 'react';
import styles from './home.module.scss';

class Home extends PureComponent {
    render() {
        return (
            <div className={styles.container}>
                Home Works!
            </div>
        )
    }
}

export default Home;
