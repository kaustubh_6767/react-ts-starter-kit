import React, { PureComponent } from "react";
import { BrowserRouter } from "react-router-dom";
import styles from './app.module.scss';
import { EveRoutes } from "../../types";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import Loader from "../../common/components/loader/loader.component";
import AppRoutes from "./app.routes";
import { Theme, withStyles, createStyles, WithStyles } from "@material-ui/core";
import CustomizedSnackbars from "../../common/components/snack-bar/snack-bar.component";
import ThemeSwitch from "../../common/components/theme-switch/theme-switch.component";

const useStyles = ((theme: Theme) => createStyles({
  root: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.primary,
  },
  header: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.primary,
  }
}));

type Props = {
  common: EveRoutes.CommonStore;
  login: EveRoutes.LoginStore;
} & WithStyles<typeof useStyles>;

class App extends PureComponent<Props> {

  constructor(props: Props) {
    super(props);
    document.title = 'EveRoutes';
  }

  render() {
    const { common: { loader }, login: { isAuthenticated }, classes } = this.props;

    return (
      <div className={`${styles.container} ${classes.root}`}>
        <BrowserRouter basename="/">
          <AppRoutes isAuthenticated={isAuthenticated} />
        </BrowserRouter>
        {loader && <Loader />}
        <CustomizedSnackbars message="Hello" show={true} variant="success" />
        <div className={styles.themeSwitch}>
          <ThemeSwitch />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: EveRoutes.Store) => {
  return {
    common: { ...state.common },
    login: { ...state.login }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(App));
