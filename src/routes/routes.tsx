import React, { PureComponent } from "react";
import { Route, Switch } from "react-router-dom";
import App from "./app/app.index";

class RootRoute extends PureComponent {
    render() {
        return (
            <Switch>
                <Route path="/" exact={false} component={App} />
            </Switch>
        )
    }
}

export default RootRoute;
