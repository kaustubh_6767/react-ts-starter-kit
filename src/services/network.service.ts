import Axios, { AxiosError } from 'axios';
import { Store } from 'redux';
import { loader } from '../store/common/actions/common.actions';

export default {
    setupInterceptors: (store: Store) => {
        // Requests interceptor
        Axios.interceptors.request.use(
            function (config) {
                showLoader(store);
                return config;
            },
            function (error) {
                hideLoader(store);
                let err = prepareErrorObject(error);
                handleError(store, err);
                return Promise.reject(error);
            }
        );

        // Response interceptor
        Axios.interceptors.response.use(
            function (response) {
                hideLoader(store);
                return response;
            },
            function (error) {
                hideLoader(store);
                let err = prepareErrorObject(error);
                handleError(store, err);
                return Promise.reject(error);
            }
        );
    }
};

let count = 0;
const showLoader = (store: Store) => {
    store.dispatch(loader(true));
    count++;
};

const hideLoader = (store: Store) => {
    if (count <= 1) {
        store.dispatch(loader(false));
        count = 0;
    } else {
        count--;
    }
};

const handleError = (store: Store, err: any = {}) => {
    console.error(JSON.stringify(err));
    if (
        (err.status === 500 &&
            err.message === "INTERNAL SERVER ERROR: jwt expired!!") ||
        err.status === 401 || err.status === 403
    ) {
        // store.dispatch(clearAuth());
    }
};

/* Function to validate|prepare|modify error object */
const prepareErrorObject = (error: AxiosError) => {
    let err = error.response ? error.response.data : error;
    if (typeof err === "object") {
        err.timestamp = Date.now();
        err.config = error.config;
    } else {
        err = {};
        err.message = "Something went wrong.";
        err.timestamp = Date.now();
    }
    return err;
};