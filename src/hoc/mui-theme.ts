import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import { Overrides } from '@material-ui/core/styles/overrides';

const overrides: Overrides = {
    MuiButton: {
        label: {
            textTransform: 'capitalize'
        }
    }
};

const lightPalette: ThemeOptions = {
    typography: {
        fontSize: 16,
    },
    breakpoints: {
        keys: ['xs', 'sm', 'md', 'lg', 'xl'],
        values: {
            xs: 0, lg: 1280, sm: 600, xl: 1920, md: 960
        }
    },
    palette: {
        type: 'light',
        // text: {
        //     primary: ''
        // },
        primary: {
            main: '#1976d2',
            light: '#4791db',
            dark: '#115293',
            // contrastText: '#ffffff'
        },
        secondary: {
            main: '#dc004e',
            light: '#e33371',
            dark: '#9a0036',
            // contrastText: '#ffffff'
        },
        error: {
            main: '#f44336',
            light: '#e57373',
            dark: '#d32f2f',
            // contrastText: '#ffffff'
        },
        warning: {
            main: '#ff9800',
            light: '#ffb74d',
            dark: '#f57c00',
            // contrastText: '#ffffff'
        },
        info: {
            main: '#2196f3',
            light: '#64b5f6',
            dark: '#1976d2',
            // contrastText: '#ffffff'
        },
        success: {
            main: '#4caf50',
            light: '#81c784',
            dark: '#388e3c',
            // contrastText: '#ffffff'
        }
    },
    overrides
};

const darkPalette: ThemeOptions = {
    typography: {
        fontSize: 16
    },
    palette: {
        type: 'dark',
        background: {
            default: '#121212',
            paper: '#272727'
        },
        text: {
            primary: '#ffffff',
            disabled: '#12121226',
        },
        primary: {
            main: '#03dac6',
            contrastText: '#272727'
        },
        secondary: {
            main: '#96979c',
            contrastText: '#ffffff'
        },
        error: {
            main: '#cf6579',
            light: '#e57373',
            dark: '#d32f2f',
            contrastText: '#000000'
        },
        warning: {
            main: '#ff9800',
            light: '#ffb74d',
            dark: '#f57c00',
            // contrastText: '#ffffff'
        },
        info: {
            main: '#2196f3',
            light: '#64b5f6',
            dark: '#1976d2',
            // contrastText: '#ffffff'
        },
        success: {
            main: '#4caf50',
            light: '#81c784',
            dark: '#388e3c',
            // contrastText: '#ffffff'
        }
    },
    overrides
};

export const lightTheme = createMuiTheme(lightPalette);
export const darkTheme = createMuiTheme(darkPalette);
