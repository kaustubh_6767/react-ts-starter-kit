export namespace EveRoutes {
    export type AppTheme = 'light' | 'dark';
    export interface CommonStore {
        loader: boolean;
    }

    export interface SettingsStore {
        theme: AppTheme;
    }

    export interface LoginStore {
        isAuthenticated: boolean;
        role: string;
    }
    export interface Store {
        common: CommonStore;
        login: LoginStore;
        settings: SettingsStore;
    }
}