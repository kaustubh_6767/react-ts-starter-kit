import React from 'react';
import { EveRoutes } from '../../../types';
import { Dispatch, bindActionCreators } from 'redux';
import { switchTheme } from '../../../store/common/actions/settings.actions';
import { connect } from 'react-redux';
import Switch from '@material-ui/core/Switch';

interface Props {
    theme: EveRoutes.AppTheme;
    action__switchTheme: Function;
}

const ThemeSwitch: React.FC<Props> = ({ theme, action__switchTheme }: Props) => {
    return (
        <Switch
            checked={theme === 'dark'}
            onChange={() => theme === 'dark' ? action__switchTheme('light') : action__switchTheme('dark')}
            color="primary"
            inputProps={{ 'aria-label': 'primary checkbox' }}
        />
    )
}

const mapStateToProps = (state: EveRoutes.Store) => {
    return {
        theme: state.settings.theme
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators({
        action__switchTheme: switchTheme
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ThemeSwitch)