import React, { PureComponent } from 'react';
import { Route, Redirect, RouteProps, RouteComponentProps } from "react-router-dom";
import { connect } from 'react-redux';
import { EveRoutes } from '../../../types';
import { Dispatch, bindActionCreators } from 'redux';

export interface SecureRouteProps extends RouteProps {
  authorizedRoles: string[];
  component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

interface StateProps {
  login: EveRoutes.LoginStore;
}

interface DispatchProps {

}

type Props = StateProps & SecureRouteProps;

class SecureRouteComponent extends PureComponent<Props, {}> {

  isAuthorized = (): boolean => {
    const { authorizedRoles, login: { role } } = this.props;
    if (authorizedRoles.length === 0) {
      return true;
    }
    if (authorizedRoles.includes(role)) {
      return true;
    }
    return false;
  }

  render() {
    const { login: { isAuthenticated }, component: Component, ...rest } = this.props;
    return (
      <Route {...rest} render={(props: any) => {
        if (!isAuthenticated) {
          return (<Redirect to="/login" />)
        }
        if (!this.isAuthorized) {
          return (<Redirect to="/home" />)
        }
        return <Component {...props} />
      }} />
    )
  }
}

const mapStateToProps = (state: EveRoutes.Store, props: SecureRouteProps): StateProps => {
  return {
    login: { ...state.login }
  }
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
  return bindActionCreators({}, dispatch);
}

const SecureRoute = connect(mapStateToProps, mapDispatchToProps)(SecureRouteComponent);
export default SecureRoute;
