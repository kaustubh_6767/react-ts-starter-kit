import { createStore, applyMiddleware, compose, Store } from 'redux';
import { middleware as reduxPackMiddleware } from 'redux-pack';
import rootReducer from './reducers';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const middleware = [reduxPackMiddleware]
const persistConfig = {
    key: 'root',
    storage: storage,
    whitelist: ['settings'] // only whitelist the reducers whose data you want to persist
}

let enhancers: any[] = []
const persistedReducer = persistReducer(persistConfig, rootReducer)

if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = (window && (window as any).__REDUX_DEVTOOLS_EXTENSION__) || compose;

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension())
    }
}

let composeEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
)
const store: Store = createStore(
    persistedReducer,
    {},
    composeEnhancers
)

let persistor = persistStore(store)

const deleteStore = () => {
    return persistor.purge()
}

export {
    persistor,
    store,
    deleteStore
}
