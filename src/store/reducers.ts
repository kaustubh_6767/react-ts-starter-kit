import { combineReducers } from "redux";
import CommonReducer from "./common/reducers/common.reducer";
import LoginReducer from "./common/reducers/login.reducer";
import SettingsReducer from "./common/reducers/settings.reducer";

const rootReducer = combineReducers({
    common: CommonReducer,
    login: LoginReducer,
    settings: SettingsReducer
});

export default rootReducer;
