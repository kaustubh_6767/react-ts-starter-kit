import { EveRoutes } from "../../../types";

export const SWITCH_THEME = 'SWITCH_THEME';

export const switchTheme = (theme: EveRoutes.AppTheme) => {
    return {
        type: SWITCH_THEME,
        payload: theme
    }
}