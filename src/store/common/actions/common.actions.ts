export const LOADER = 'LOADER';

export const loader = (showLoader: boolean) => {
    return {
        type: LOADER,
        payload: showLoader
    }
}
