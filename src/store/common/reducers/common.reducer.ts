import { EveRoutes } from "../../../types";
import { AnyAction } from "redux";
import { LOADER } from "../actions/common.actions";

const CommonReducer = (state: EveRoutes.CommonStore = {
    loader: false
}, action: AnyAction): EveRoutes.CommonStore => {
    switch (action.type) {
        case LOADER:
            return {
                ...state,
                loader: action.payload
            }

        default:
            return state;
    }
}

export default CommonReducer;
