import { EveRoutes } from "../../../types";
import { AnyAction } from "redux";

const LoginReducer = (state: EveRoutes.LoginStore = {
    isAuthenticated: false,
    role: ''
}, action: AnyAction): EveRoutes.LoginStore => {
    switch (action.type) {
        default:
            return state;
    }
}

export default LoginReducer;
