import { EveRoutes } from "../../../types";
import { AnyAction } from "redux";
import { SWITCH_THEME } from "../actions/settings.actions";

const SettingsReducer = (state: EveRoutes.SettingsStore = {
    theme: 'dark'
}, action: AnyAction): EveRoutes.SettingsStore => {
    switch (action.type) {

        case SWITCH_THEME:
            return {
                ...state,
                theme: action.payload
            }

        default:
            return state;
    }
}

export default SettingsReducer;
